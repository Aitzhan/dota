﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarracksController : MonoBehaviour
{
    public Spawner spawner;
    [SerializeField]
    private List<UnitController> units;
    public Button createButton;
    private UIManager uiManager;
    private AudioSource source;
    private void Start()
    {
        UpdateListUnits();
        spawner = FindObjectOfType<Spawner>();
        createButton.onClick.AddListener(SwordBarracks);
        uiManager = FindObjectOfType<UIManager>();
        source = GetComponent<AudioSource>();
    }
    private void OnMouseDown()
    {
        transform.GetChild(1).gameObject.SetActive(true);
/*        if (gameObject.tag == "SwordBarracks") SwordBarracks();
        if (gameObject.tag == "ArchersBarracks") ArchersBarracks();*/
    }

    public void SwordBarracks()
    {
        if (GameManager.Money >= GameManager.ShopUnit)
        {
            StartCoroutine(CreateSwordUnitTimer());
            GameManager.Money -= GameManager.ShopUnit;
            uiManager.MoneyTextUpdate();
        }
        else
        {
            uiManager.NotEnoughMoney();
        }
            
        print("SwordBarracks");
    }
   
    public void CloseUI()
    {
        if (spawner.SwordPrefab() != null)
            spawner.SwordPrefab().transform.GetChild(1).gameObject.SetActive(false);
        if (spawner.ArchersPrefab() != null)
            spawner.ArchersPrefab().transform.GetChild(1).gameObject.SetActive(false);
    }
    public void UpdateListUnits()
    {
        units.Clear();
        units.AddRange(FindObjectsOfType<UnitController>());
    }
    public void ArchersBarracks()
    {
        
    }
    IEnumerator CreateSwordUnitTimer()
    {
        yield return new WaitForSeconds(3f);
        spawner.SpawnMyUnit();
        print("Coroutine");
    }

    public void UpgradeMyUnits()
    {
        if (GameManager.Money >= GameManager.UpgradeLevelMyUnit)
        {
            StartCoroutine(Study());
        }
        else
        {
            uiManager.NotEnoughMoney();
        }
    }
    IEnumerator Study()
    {
        yield return new WaitForSeconds(5f);
        GameManager.Money -= GameManager.UpgradeLevelMyUnit;
        uiManager.MoneyTextUpdate();
        GameManager.UnitDamage += 2;
        source.Play();
    }
}
