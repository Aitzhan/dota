﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public Text moneyText;
    // Start is called before the first frame update
    void Start()
    {
        MoneyTextUpdate();
    }

  public void MoneyTextUpdate()
    {
        moneyText.text = $"Бабки: {GameManager.Money}";
    }
    public void NotEnoughMoney()
    {
        moneyText.gameObject.GetComponent<AudioSource>().Play();
    }
}
