﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameManager
{
   private static int money=200;
    private static int myUnitCost = 50;
    private static int damage = 20;
    private static int upgradeLevelMyUnit = 10;
    public static int Money
    {
        get { return money; }
        set { money = value; Debug.Log(money); }
    }
    public static int ShopUnit
    {
        get { return myUnitCost; }
    }
    public static int UnitDamage
    {
        get { return damage; }
        set { damage = value; }
    }
    public static int UpgradeLevelMyUnit
    {
        get { return upgradeLevelMyUnit; }
        set { upgradeLevelMyUnit = value; }
    }
}
