﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllOutlineUnitController : MonoBehaviour
{
    public RectTransform selectionBox;

    private Vector2 startPos;

    private Camera mainCamera;
    private UnitController[] _allUnits;
    // Start is called before the first frame update
    void Start()
    {
        _allUnits = FindObjectsOfType<UnitController>();

        mainCamera = Camera.main;
        

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            startPos = Input.mousePosition;
          
        }

        if (Input.GetMouseButtonUp(0))
        {
            ReleaseSelectionBox();
        }

        if (Input.GetMouseButton(0))
        {
            UpdateSelectionBox(Input.mousePosition);
        }
    }
    private void OnMouseDown()
    {
        for (int i = 0; i < _allUnits.Length; i++)
        {
            _allUnits[i].OutlineUnit();
        }
       
    }


    void ReleaseSelectionBox()
    {
        selectionBox.gameObject.SetActive(false);

        Vector2 min = selectionBox.anchoredPosition - (selectionBox.sizeDelta / 2);
        Vector2 max = selectionBox.anchoredPosition + (selectionBox.sizeDelta / 2);

        _allUnits = FindObjectsOfType<UnitController>();

        foreach (UnitController unit in _allUnits)
        {
            Vector3 screenPos = mainCamera.WorldToScreenPoint(unit.transform.position);

            if (screenPos.x > min.x && screenPos.x < max.x && screenPos.y > min.y && screenPos.y < max.y)
            {
                unit.transform.GetChild(0).gameObject.SetActive(true);
            }

        }


    }

    void UpdateSelectionBox(Vector2 currentMousePos)
    {
        if (!selectionBox.gameObject.activeInHierarchy)
            selectionBox.gameObject.SetActive(true);


        float width = currentMousePos.x - startPos.x;
        float height = currentMousePos.y - startPos.y;

        selectionBox.sizeDelta = new Vector2(Mathf.Abs(width), Mathf.Abs(height));

        selectionBox.anchoredPosition = startPos + new Vector2(width / 2, height / 2);

    }
}
