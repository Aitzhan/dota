﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseController : MonoBehaviour
{
    private int hp = 1000;
    public TextMesh txt;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    void MyHealth(int _hp)
    {
        hp -= _hp;
        txt.text = hp.ToString();
        if (hp <= 0) Application.LoadLevel(0);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "SwordEnemy") MyHealth(100);
    }
}
