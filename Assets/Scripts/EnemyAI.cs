﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class EnemyAI : MonoBehaviour
{
    public int hp = 10;
    public float speed;

    public List<float> distSeeUnit;
    public float distBase;

    private Transform basePosition;
    public List<Transform> units;

    private bool seeUnity;

    private NavMeshAgent agent;
    // Start is called before the first frame update
    void Awake()
    {
        basePosition = GameObject.FindGameObjectWithTag("Base").transform;
        agent = GetComponent<NavMeshAgent>();
        AddDistance();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        distBase = Vector3.Distance(transform.position, basePosition.position);
        if (distBase > 5)
        {
            if (units.Count == 0)
            {

                Vector3 direction = basePosition.position - transform.position;
                transform.rotation = Quaternion.LookRotation(direction);
                //transform.position = Vector3.MoveTowards(transform.position, basePosition.position, speed * Time.deltaTime);
                agent.SetDestination(basePosition.position);
            }
            else if (units.Count > 0)
            {
                if (GetMinIndex() < units.Count && GetMinIndex() >= 0)
                {
                    agent.SetDestination(units[GetMinIndex()].position);
                    // transform.position = Vector3.MoveTowards(transform.position, units[GetMinIndex()].position, speed * Time.deltaTime);
                    Vector3 direction = units[GetMinIndex()].position - transform.position;
                    transform.rotation = Quaternion.LookRotation(direction);
                }
            }
        }
    }
    public int GetMinIndex()
    {
 
        int index = 0;
        try
        {
            float min = Vector3.Distance(transform.position, units[0].position);

            for (int i = 1; i < units.Count; i++)
            {
                if (Vector3.Distance(transform.position, units[i].position) < min)
                {
                    index = i;
                }
            }
            return index;
        }
        catch (System.Exception)
        {
            AddDistance();
            return -1;
        }
        
    }
    public void AddDistance()
    {
        units.Clear();
        distSeeUnit.Clear();
        foreach (GameObject unit in GameObject.FindGameObjectsWithTag("Units"))
        {
            units.Add(unit.transform);
        }
        for (int i = 0; i < units.Count; i++)
        {
            distSeeUnit.Add(Vector3.Distance(transform.position, units[i].position));
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Units"|| other.tag=="Base")
        {
            transform.GetComponent<Animator>().enabled = true;
        }
        if (other.tag == "SwordEnemy") GetDamage(GameManager.UnitDamage);
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Units" || other.tag == "Base")
        {
            transform.GetComponent<Animator>().enabled = false;
        }
    }
    private void OnDestroy()
    {
        GameManager.Money += 10;
        FindObjectOfType<UIManager>().MoneyTextUpdate();
    }
    void GetDamage(int getDamage)
    {
        if (hp > 10) {
            hp -= getDamage; 
        }
        else {
            Destroy(gameObject);
        }
    }
}
