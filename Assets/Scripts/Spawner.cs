﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Spawner : MonoBehaviour
{
    public GameObject enemyPrefab;
    public GameObject myUnitPrefab;

    public Transform enemySpawnPoint;
    public Transform myUnitSpawnPoint;

    private List<EnemyAI> enemyAI=new List<EnemyAI>();
    public int wave;

    [SerializeField]
    private GameObject swordsmanBasePrefab;
    [SerializeField]
    private Transform swordsmanBasePosition;
    [SerializeField]
    private GameObject archersBasePrefab;
    [SerializeField]
    private Transform archersBasePosition;
    [SerializeField]
    private GameObject fontainPrefab;
    [SerializeField]
    private Transform fontainPosition;
    // Start is called before the first frame update
    void Start()
    {          
        InvokeRepeating("SpawnEnemyUnits", 0, 60);

    }
   
    void SpawnEnemy()
    {
        Instantiate(enemyPrefab, enemySpawnPoint.position, Quaternion.identity);
    }
    public void SpawnMyUnit()
    {
        GameObject unit=null;
        if (swordsmanBasePrefab != null)
        {
           unit = Instantiate(myUnitPrefab, swordsmanBasePrefab.transform.position, Quaternion.identity);
           unit.GetComponent<UnitController>().AudioPlayCreateUnit();
        }
        Transform spawnPos = swordsmanBasePrefab.transform;
        for (int i = 0; i < swordsmanBasePrefab.transform.childCount; i++)
        {
            if (swordsmanBasePrefab.transform.GetChild(i).tag == "SpawnUnitPoint")
            {
                spawnPos = swordsmanBasePrefab.transform.GetChild(i);
                break;
            }
        }
        unit.GetComponent<NavMeshAgent>().SetDestination(spawnPos.position);

        enemyAI.Clear();
        enemyAI.AddRange(FindObjectsOfType<EnemyAI>());

        for (int i = 0; i < enemyAI.Count; i++)
        {
            enemyAI[i].AddDistance();
        }
        FindObjectOfType<BarracksController>().UpdateListUnits();
        print("Spawn");
    }

    public void SpawnEnemyUnits()
    {
        StartCoroutine(TimerWave());
    }

    IEnumerator TimerWave()
    {
        for (int i = 0; i < 10; i++)
        {
            yield return new WaitForSeconds(1f);
            SpawnEnemy();            
        }

    }
    public void SwordTower()
    {
        if(swordsmanBasePrefab==null)
        {
            swordsmanBasePrefab = Instantiate(Resources.Load<GameObject>("SwordsmanBase"),swordsmanBasePosition.position,Quaternion.identity);
        }
    }
    public void ArcherTower()
    {
        if (archersBasePrefab == null)
        {
            archersBasePrefab = Instantiate(Resources.Load<GameObject>("ArchorsBase"), archersBasePosition.position, Quaternion.identity);
        }
    }
    public void FontainTower()
    {
        if (fontainPrefab == null)
        {
            fontainPrefab = Instantiate(Resources.Load<GameObject>("Fontain"), fontainPosition.position, Quaternion.identity);
        }
    }
    public GameObject SwordPrefab()
    {
        return swordsmanBasePrefab;
    }
    public GameObject ArchersPrefab()
    {
        return archersBasePrefab;
    }
}
