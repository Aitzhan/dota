﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
public class UnitController : MonoBehaviour
{
    private int hp = 100;
  
    public List<EnemyAI> enemyAI = new List<EnemyAI>();
    private List<GameObject> units = new List<GameObject>();
    public Text damageText;
    public Scrollbar hpLine;
    private NavMeshAgent agent;
    public LayerMask layer;
    public AudioClip[] clips;
    private AudioSource source;
    private int random;
    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        enemyAI.AddRange(FindObjectsOfType<EnemyAI>());
        units.AddRange(GameObject.FindGameObjectsWithTag("Units"));
        damageText.text = "Урон: " + GameManager.UnitDamage.ToString();
       

    }
    private void Update()
    {
        if (Input.GetMouseButtonDown(1) && transform.GetChild(0).gameObject.activeSelf) Movement();
    }
   
    void UpdateUI()
    {
        damageText.text ="Урон: " + GameManager.UnitDamage.ToString();
        hpLine.size -= 0.2f;

    }
    private void OnMouseDown()
    {
        OutlineUnit();
        transform.GetChild(0).gameObject.SetActive(true);
    }
    public void OutlineUnit()
    {
        units.Clear();
        units.AddRange(GameObject.FindGameObjectsWithTag("Units"));
        foreach (GameObject unit in units)
        {
            unit.transform.GetChild(0).gameObject.SetActive(false);
        }
    }
    public void GetDamage(int damage)
    {
        hp -= damage;
        UpdateUI();
        if (hp <= 0) Destroy(gameObject);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "SwordEnemy") GetDamage(20);
        if (other.tag == "Enemy")
        {
            gameObject.GetComponent<Animator>().enabled = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Enemy")
        {
            print("Exit");
            gameObject.GetComponent<Animator>().enabled = false;
        }
    }
    private void OnDestroy()
    {
        if (FindObjectOfType<BarracksController>()!=null)
            FindObjectOfType<BarracksController>().UpdateListUnits();
        for (int i = 0; i < enemyAI.Count; i++)
        {
            enemyAI[i].AddDistance();
        }
       
    }
    void Movement()
    {
        Ray myRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;
        if(Physics.Raycast(myRay,out hitInfo, 100,layer))
        {
            agent.SetDestination(hitInfo.point);
            random = Random.Range(0, 3);
            source = GetComponent<AudioSource>();
            source.clip = clips[random];
            
            if(!source.isPlaying)
            source.Play();

        }
    }
    public void AudioPlayCreateUnit()
    {
        random = Random.Range(0, 3);
        source = GetComponent<AudioSource>();
        source.clip = clips[random];
        source.Play();
    }
    void Attack()
    {
        
    }
}
