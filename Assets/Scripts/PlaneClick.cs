﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneClick : MonoBehaviour
{
    public List<UnitController> unitControllers;
    public Spawner spawner;
    private void Start()
    {
        unitControllers.AddRange(FindObjectsOfType<UnitController>());
    }
    private void OnMouseDown()
    {
        print("Plane click");
        for (int i = 0; i < unitControllers.Count; i++)
        {
            unitControllers[i].OutlineUnit();
        }
        if (spawner.SwordPrefab() != null)
            spawner.SwordPrefab().transform.GetChild(1).gameObject.SetActive(false);
        if (spawner.ArchersPrefab() != null)
            spawner.ArchersPrefab().transform.GetChild(1).gameObject.SetActive(false);
    }
}
